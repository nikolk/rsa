#pragma once
#include "EuclideanAlgorithm.h"
#include <stdlib.h>
#include <time.h> 
#include <iostream>
#include <vector>
#include <string>

#define NUM_OPTIONS 100

class RsaAlgorithm
{
private:
	int _n;
	int _e;
	int _d;
	int _phi_n;
	int _numOptions;
	EuclideanAlgorithm _logic;

	int findRelativePrimeNumber();
	long int phi(int q, int p);

public:
	RsaAlgorithm(int q, int p);
	RsaAlgorithm(int q, int p, int num_options);
	void initKeys();
	long long int encrypt(short msg);
	long long int decrypt(short msg);

	//getters
	int getE();
	int getN();
};