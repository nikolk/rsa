#pragma once
#include <stack>
#include <iostream>

class EuclideanAlgorithm
{
protected:
	std::stack<std::string> _equations;

public:
	int gcd(int num1, int num2);
	int extendedEuclideanAlgorithm(int num1, int num2, int* num1_tag, int* num2_tag);
	
};