#include "EuclideanAlgorithm.h"

int EuclideanAlgorithm::gcd(int num1, int num2)
{
	 int reminder = num1 % num2;
	if (reminder == 0)
		return 0;

	 int result = gcd(num2, reminder);
	if (result == 0)
		return reminder;
	else
		return result;
}

 int EuclideanAlgorithm::extendedEuclideanAlgorithm( int num1,  int num2,  int* x_tag,  int* y_tag)
{
	 int prev_x, prev_y;
	if (num1 == 0)
	{
		*x_tag = 0;
		*y_tag = 1;
		return num2;
	}
	 int result = extendedEuclideanAlgorithm(num2 % num1, num1, &prev_x, &prev_y);
	*x_tag = prev_y - (num2 / num1) * prev_x;
	*y_tag = prev_x;

	return result;
}