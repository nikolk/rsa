#pragma once
#include <iostream>
#include <string>

class Interface
{
public:
	void welcome();
	void init_q_p(int* q, int* p);
	int menu(void);
	void publicKey(int n, int e);
	int getNumber();
	std::string getMessage();
};