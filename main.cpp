#include <iostream>
#include <vector>
#include <stack>
#include <string>
#include <stdlib.h>
#include <time.h> 
#include "RsaAlgorithm.h"
#include "Interface.h"

#define NUM_OPTIONS 1000 // maybe randomize this number too
enum OPTIONS{GENERATE=1, ENCRYPT_NUM, DECRYPT_NUM, EXIT};

int main(void)
{
	int num;
	int decrypted;
	int choice;
	int encrypted;
	int q = 0;
	int p = 0;
	std::string msg;
	std::string msg_encrypted;
	std::string msg_decrypted;
	bool flag = false;
	Interface gui;
	RsaAlgorithm* rsa(nullptr);
	gui.welcome();
	do {
		choice = gui.menu();
		switch (choice)
		{
		case GENERATE:
			gui.init_q_p(&q, &p);
			rsa = new RsaAlgorithm(q, p);
			rsa->initKeys();
			gui.publicKey(rsa->getN(), rsa->getE());
			flag = true;
			break;
		case ENCRYPT_NUM:
			if (flag)
			{
				num = gui.getNumber();
				encrypted = rsa->encrypt(num);
				std::cout << "encrypted message: " << encrypted << std::endl;
			}
			else
				std::cout << "you need to generate keys first";
			break;
		case DECRYPT_NUM:
			num = gui.getNumber();
			decrypted = rsa->decrypt(num);
			std::cout << "decrypted message: " << decrypted << std::endl;
			break;
		}
		} while (choice >= GENERATE && choice < EXIT);


		system("pause");
		return 0;
	}