#include "Interface.h"

void Interface::welcome()
{
	//generated in http://patorjk.com/software/taag/#p=display&f=Graffiti&t=Type%20Something%20
	std::cout << "RRRRRRRRRRRRRRRRR      SSSSSSSSSSSSSSS               AAA\n"
		<< "R::::::::::::::::R   SS:::::::::::::::S             A:::A\n"
		<< "R::::::RRRRRR:::::R S:::::SSSSSS::::::S            A:::::A\n"
		<< "RR:::::R     R:::::RS:::::S     SSSSSSS           A:::::::A\n"
		<< "  R::::R     R:::::RS:::::S                      A:::::::::A\n"
		<< "  R::::R     R:::::RS:::::S                     A:::::A:::::A\n"
		<< "  R::::RRRRRR:::::R  S::::SSSS                 A:::::A A:::::A\n"
		<< "  R:::::::::::::RR    SS::::::SSSSS           A:::::A   A:::::A\n"
		<< "  R::::RRRRRR:::::R     SSS::::::::SS        A:::::A     A:::::A\n"
		<< "  R::::R     R:::::R       SSSSSS::::S      A:::::AAAAAAAAA:::::A\n"
		<< "  R::::R     R:::::R            S:::::S    A:::::::::::::::::::::A\n"
		<< "  R::::R     R:::::R            S:::::S   A:::::AAAAAAAAAAAAA:::::A\n"
		<< "RR:::::R     R:::::RSSSSSSS      S:::::S A:::::A             A:::::A\n"
		<< "R::::::R     R:::::RS::::::SSSSSS:::::SA:::::A                A:::::A\n"
		<< "R::::::R     R:::::RS:::::::::::::::SSA:::::A                  A:::::A\n"
		<< "RRRRRRRR     RRRRRRR SSSSSSSSSSSSSSS AAAAAAA                    AAAAAAA\n";


	std::cout << "\n\n\n" << "Welcome to RSA program !!\n\n";
}

void Interface::init_q_p(int* q, int* p)
{
	std::cout << "enter p: ";
	std::cin >> *p;
	std::cout << "enter q: ";
	std::cin >> *q;
}

int Interface::menu(void)
{
	int choice = 0;
	std::cout << "Choose action to preform:" << std::endl
		<< "1. generate public & private key" << std::endl
		<< "2. encrypt number" << std::endl
		<< "3. decrypt number" << std::endl
		<< "4. exit" << std::endl << "enter option: ";
	std::cin >> choice;
	std::cout << std::endl;
	return choice;
}

void Interface::publicKey(int n, int e)
{
	std::cout << "n = " << n << std::endl << "e = " << e << std::endl;
}

int Interface::getNumber()
{
	int msg;
	std::cout << "enter number: ";
	std::cin >> msg;
	return msg;
}

std::string Interface::getMessage()
{
	std::string msg;

	//clear the buffer
	std::cin.ignore();

	std::cout << "enter message: ";
	std::getline(std::cin, msg);
	return msg;
}
