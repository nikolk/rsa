# include "RsaAlgorithm.h"

RsaAlgorithm::RsaAlgorithm(int q, int p)
{
	_n = q * p;
	_phi_n = phi(q, p);
	_e = 0;
	_numOptions = 1000; // default
}

RsaAlgorithm::RsaAlgorithm(int q, int p, int num_options) : RsaAlgorithm(q, p)
{
	_numOptions = num_options;
}

 int RsaAlgorithm::findRelativePrimeNumber()
{
	std::vector< int> options(0);

	for (int i = 2; i < _phi_n; i++)
	{
		if (_logic.gcd(i, _phi_n) == 1 && _logic.gcd(i, _n) == 1)
			options.push_back(i);
	}

	int index = rand() % options.size();
	return options[index];
}

long int RsaAlgorithm::phi(int q, int p)
{
	return (q - 1) * (p - 1);
}

void RsaAlgorithm::initKeys()
{
	 int y;
	srand(time(NULL));
	_e = findRelativePrimeNumber();
	_logic.extendedEuclideanAlgorithm(_e, _phi_n, &_d, &y);
	_d = _d < 0 ? _phi_n + _d : _d;
	//_d += _phi_n;

}

long long int RsaAlgorithm::encrypt(short msg)
{
	double temp = pow(msg, _e);
	temp = fmod(temp, _n);
	return temp;
}

long long int RsaAlgorithm::decrypt(short msg)
{
	double temp = pow(msg, _d);
	return fmod(temp, _n);
}

//getters
int RsaAlgorithm::getE()
{
	return _e;
}

int RsaAlgorithm::getN()
{
	return _n;
}